package depaul.se459.sensor;

import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;

import depaul.se459.sensor.CellCondition;
import depaul.se459.sensor.CellWalls;
import depaul.se459.sensor.Sensor;
import depaul.se459.sensor.SensorCell;




public class SensorDataTest extends TestCase {
	public SensorDataTest(String name){
		super(name);
	}
	public void testReadInJson(){
		SensorData sensorData = new SensorData();
		List<SensorCell> cellList = new ArrayList<SensorCell>();
		cellList = sensorData.readInJson("layout.json");
		
		for(int i=0; i<cellList.size(); i++){
			assertTrue(cellList.get(i) != null);
		}
		
		SensorCell sc = cellList.get(0);
		assertTrue(sc.getCordX() == 0);
		assertTrue(sc.getCordY() == 0);
		assertTrue(sc.getfloorSurface().equals("bare"));
	}
}
