package depaul.se459.sensor;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.sensor.Sensor;
import depaul.se459.sensor.SensorCell;
import depaul.se459.sensor.SensorCellFact;

public class SensorCellTest extends TestCase {
	public SensorCellTest(String name){
		super(name);
	}
	public void testGetSetCellWalls(){
		SensorCell sc1 = SensorCellFact.createCell();
		
		sc1.setCellWalls("OPEN","OBSTACLE","OPEN","STAIRS");
		
		CellWalls cw1 = sc1.getCellWalls();
		
		assertTrue(cw1.getNorthWall().equals(CellCondition.OPEN));
		assertTrue(cw1.getSouthWall().equals(CellCondition.OBSTACLE));
		assertTrue(cw1.getEastWall().equals(CellCondition.OPEN));
		assertTrue(cw1.getWestWall().equals(CellCondition.STAIRS));
	}
	public void testGetSetCoordinates(){
		SensorCell sc1 = SensorCellFact.createCell();
		
		sc1.setCordXY(3,7);
		
		assertTrue(sc1.getCordX() == 3);
		assertTrue(sc1.getCordY() == 7);
	}
	public void testGetSetFloorSurface(){
		SensorCell sc1 = SensorCellFact.createCell();
		
		sc1.setfloorSurface("bare");
		assertTrue(sc1.getfloorSurface().equals("bare"));
		
		sc1.setfloorSurface("HIGH_PILE");
		assertTrue(sc1.getfloorSurface().equals("HIGH_PILE"));
		
		sc1.setfloorSurface("LOW_PILE");
		assertTrue(sc1.getfloorSurface().equals("LOW_PILE"));
		
	}
	public void testGetSetUnitOfDirt(){
		SensorCell sc1 = SensorCellFact.createCell();
		
		sc1.setUnitOfDirt(10);
		assertTrue(sc1.getUnitOfDirt() == 10);
		
		sc1.setUnitOfDirt(0);
		assertTrue(sc1.getUnitOfDirt() == 0);
	}
	public void testGetSetChargingStation(){
		SensorCell sc1 = SensorCellFact.createCell();
		
		sc1.setChargerStationLocation("yes");
		assertTrue(sc1.getChargerStationLocation());
		
		sc1.setChargerStationLocation("no");
		assertFalse(sc1.getChargerStationLocation());
	}
}
