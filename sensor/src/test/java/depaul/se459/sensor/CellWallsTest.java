package depaul.se459.sensor;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.sensor.Sensor;
import depaul.se459.sensor.SensorCell;
//import depaul.se459.controller.

public class CellWallsTest extends TestCase {
	
	public CellWallsTest(String name){
		super(name);
	}
	public void testGetEastWall(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OPEN");
			
		assertTrue(cw.getEastWall().equals(CellCondition.OPEN));
		assertTrue(cw2.getEastWall().equals(CellCondition.OBSTACLE));
		assertTrue(cw3.getEastWall().equals(CellCondition.STAIRS));
	}
	public void testGetNorthWall(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OBSTACLE");
			
		assertTrue(cw.getNorthWall().equals(CellCondition.OPEN));
		assertTrue(cw2.getNorthWall().equals(CellCondition.OPEN));
		assertTrue(cw3.getNorthWall().equals(CellCondition.OBSTACLE));
	}
	public void testGetSouthWall(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OBSTACLE");
			
		assertTrue(cw.getSouthWall().equals(CellCondition.OPEN));
		assertTrue(cw2.getSouthWall().equals(CellCondition.STAIRS));
		assertTrue(cw3.getSouthWall().equals(CellCondition.OBSTACLE));
	}
	public void testGetWestWall(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OBSTACLE");
			
		assertTrue(cw.getWestWall().equals(CellCondition.OPEN));
		assertTrue(cw2.getWestWall().equals(CellCondition.OPEN));
		assertTrue(cw3.getWestWall().equals(CellCondition.OBSTACLE));
	}
	public void testIsNorthOpen(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OBSTACLE");
			
		assertTrue(cw.isNorthOpen());
		assertTrue(cw2.isNorthOpen());
		assertFalse(cw3.isNorthOpen());
	}
	public void testIsEastOpen(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OBSTACLE");
			
		assertTrue(cw.isEastOpen());
		assertFalse(cw2.isEastOpen());
		assertFalse(cw3.isEastOpen());
	}
	public void testIsSouthOpen(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OBSTACLE");
			
		assertTrue(cw.isSouthOpen());
		assertFalse(cw2.isSouthOpen());
		assertFalse(cw3.isSouthOpen());
	}
	public void testIsWestOpen(){
		CellWalls cw = new CellWalls("OPEN","OPEN","OPEN", "OPEN");
		CellWalls cw2 = new CellWalls("OPEN","STAIRS","OBSTACLE","OPEN");
		CellWalls cw3 = new CellWalls("OBSTACLE","OBSTACLE","STAIRS","OBSTACLE");
			
		assertTrue(cw.isWestOpen());
		assertTrue(cw2.isWestOpen());
		assertFalse(cw3.isWestOpen());
	}
}
