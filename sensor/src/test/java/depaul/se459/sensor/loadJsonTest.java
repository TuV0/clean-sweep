package depaul.se459.sensor;


import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.sensor.CellCondition;
import depaul.se459.sensor.CellWalls;
import depaul.se459.sensor.Sensor;
import depaul.se459.sensor.SensorCell;




/**
 * A test class associated with user story 2.2:
 * 
 * I, as a developer,want the Sensor Simulator to be able to read the pre-defined floor plans so it can "play back"
 */
public class loadJsonTest extends TestCase {
	public loadJsonTest(String name){
		super(name);
	}
	
	/**
	 * The goal of this test is to make sure that data is being imported, and that each section of the data is correct. 
	 * The data that needs to be checked is:
	 *  - surface
	 *  - coordinates
	 *  - cell walls
	 *  - charging station
	 */
	public void testJsonImport(){
		/*Sensor sense  = Sensor.getInstance();
		sense.loadDataToSensor();
		List<SensorCell> c = sense.getCellList();
		
		//Check that data was imported as well as the floor type.
		for(SensorCell sc : c){
			Assert.assertTrue(sc != null);
			Assert.assertEquals(sense.getFloorType(sc.getCordX(), sc.getCordY()),FloorType.bare);
		}
		CellWalls cw = sense.getCell(0, 0);
		//check to see that cell walls was imported
		Assert.assertTrue(cw.getNorthWall() == CellCondition.OBSTACLE);
		Assert.assertTrue(cw.getEastWall() == CellCondition.OPEN);
		Assert.assertTrue(cw.getSouthWall() == CellCondition.OPEN);
		Assert.assertTrue(cw.getWestWall() == CellCondition.OBSTACLE);
		//Check to see that dirt was imported
		Assert.assertTrue(sense.isDirty(sense.getCellFromSensor(0, 0)));
		SensorCell sc1 = sense.getCellFromSensor(0,0);
		//System.out.println(sc1.getChargerStationLocation());
		Assert.assertEquals(sc1.getChargerStationLocation(),false);*/
	}
}
