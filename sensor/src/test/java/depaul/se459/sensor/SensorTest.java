package depaul.se459.sensor;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.sensor.Sensor;
import depaul.se459.sensor.SensorCell;

public class SensorTest extends TestCase {
	public SensorTest(String name){
		super(name);
	}
	public void testIsCharginStation(){
		Sensor sense = Sensor.getInstance();
		
		//Testing isCharginStation(x,y) on layout.json
		assertTrue(sense.isCharginStation(0,0));
		assertTrue(sense.isCharginStation(0,3));
		assertTrue(sense.isCharginStation(4,5));		
	}
	public void testIsDirty(){
		Sensor sense = Sensor.getInstance();

		//Testing isDirty(x,y) on layout.json
		assertTrue(sense.isDirty(1,0));
		assertTrue(sense.isDirty(2,0));
		assertTrue(sense.isDirty(3,0));
		assertTrue(sense.isDirty(4,0));
		assertTrue(sense.isDirty(0,1));
		assertTrue(sense.isDirty(1,1));
		assertTrue(sense.isDirty(2,1));
		assertTrue(sense.isDirty(3,1));
		assertTrue(sense.isDirty(0,3));
		assertTrue(sense.isDirty(4,3));
		assertTrue(sense.isDirty(0,4));
		assertTrue(sense.isDirty(1,4));
	}
	public void testExtractDirt(){
		Sensor sense = Sensor.getInstance();

		
		//Testing extractDirt(x,y,unit) on layout.json
		sense.extractDirt(1,0, 9);
		assertTrue(sense.isDirty(1,0));
		sense.extractDirt(1,0,1);
		assertFalse(sense.isDirty(1,0));
		
		sense.extractDirt(2,0,11);
		assertFalse(sense.isDirty(2,0));
		
		sense.extractDirt(3,0,-1);
		assertTrue(sense.isDirty(3,0));
	}
	public void testGetFloorType(){
		Sensor sense = Sensor.getInstance();
		
		String floortype = sense.getFloorType(4,0);
		assertTrue(floortype.equals("bare"));
		
		floortype = sense.getFloorType(1,0);
		assertTrue(floortype.equals("bare"));
		
		floortype = sense.getFloorType(3,3);
		assertTrue(floortype.equals("LOW_PILE"));
		
		floortype = sense.getFloorType(0,3);
		assertTrue(floortype.equals("LOW_PILE"));
	}
	public void testCanMoveNorth(){
		Sensor sense = Sensor.getInstance();
		
		assertTrue(sense.canMoveNorth(0,5));
		assertTrue(sense.canMoveNorth(4,3));
		
		assertFalse(sense.canMoveNorth(0,0));
		assertFalse(sense.canMoveNorth(1,5));
		
	}
	public void testCanMoveEast(){
		Sensor sense = Sensor.getInstance();
		assertFalse(sense.canMoveEast(0,5));
		assertTrue(sense.canMoveNorth(4,3));
		
		assertTrue(sense.canMoveEast(0,0));
		assertTrue(sense.canMoveEast(1,5));
		
		
	}
	public void testCanMoveSouth(){
		Sensor sense = Sensor.getInstance();
		
		assertFalse(sense.canMoveSouth(0,5));
		assertTrue(sense.canMoveSouth(4,3));
		
		assertTrue(sense.canMoveSouth(0,0));
		assertFalse(sense.canMoveSouth(1,5));
		
	}
	public void testCanMoveWest(){
		Sensor sense = Sensor.getInstance();
		
		assertFalse(sense.canMoveWest(0,5));
		assertTrue(sense.canMoveWest(4,3));
		
		assertFalse(sense.canMoveWest(0,0));
		assertTrue(sense.canMoveWest(1,5));
		
		
	}
}
