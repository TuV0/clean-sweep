package depaul.se459.sensor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * @author tuvo
 *
 */
public class Sensor{
	final static Logger logger = Logger.getLogger(Sensor.class);
	private static List<SensorCell> cellLists;
	private String jsonFile;
	private SensorData sensorData;
	
	private static Sensor instance = null;
	   protected Sensor() {
		loadDataToSensor();
	   }
	   public static Sensor getInstance() {
	      if(instance == null) {
	         instance = new Sensor();
	      }
	      return instance;
	   }
	   
	   private void loadDataToSensor(){
		   try{
			   this.jsonFile ="layout.json";

				cellLists = new ArrayList<SensorCell>();		
				sensorData = new SensorData();
				cellLists = sensorData.readInJson(jsonFile);
				logger.fatal("sensor working as excepted:");
			   
		   }catch(Exception e){
			   
				logger.fatal("sensor not functioning :" + e);
		   }
					
		}	
			
		public SensorCell getCellFromSensor(int x, int y){
			Iterator<SensorCell> cellIterator = cellLists.iterator();
			SensorCell cell = null;
			
			while(cellIterator.hasNext()){			
				SensorCell cellObj = cellIterator.next();
				
				if(cellObj.getCordX() == x && cellObj.getCordY() == y){
					cell = cellObj;
				}	
			}
			return cell;
			
		}
				
		/**
		 * This method locate the Cell  base on coordinates and return cellWalls 
		 * @param x
		 * @param y
		 * @return
		 */
		private CellWalls getCell (int x, int y){
			CellWalls cWalls = getCellFromSensor(x,y).getCellWalls();
			return cWalls;		
		}
		
		public boolean isCharginStation (int x, int y){
			SensorCell sc = getCellFromSensor(x,y);
			return sc.getChargerStationLocation();
		}
		
		public boolean isDirty(int x, int y) {
			
			SensorCell sc = getCellFromSensor(x,y);
			
			if(sc.getUnitOfDirt() > 0)
				return true;
			else
				return false;
		}
				
		public void extractDirt(int x, int y, int unit){
			SensorCell sc = getCellFromSensor(x,y);
			int dirtUnit =  sc.getUnitOfDirt();

			//System.out.println("Sensor: cell("+x+","+y+"):"+dirtUnit);

			sc.setUnitOfDirt(dirtUnit - unit);
		}
		
		/**
		 * This method locate the Cell base coordinates and return the type of surface
		 */
		public String getFloorType(int x, int y) {
			String flrtype = getCellFromSensor(x,y).getFloorSuface();
			return flrtype;
		}
		
		
		public boolean canMoveNorth(int x, int y){
			
			CellWalls cellWall = getCell(x,y);
			
			if(cellWall.isNorthOpen())
				return true;
			else
				return false;
		}
		
		public boolean canMoveEast(int x, int y){
			CellWalls cellWall = getCell(x,y);
			
			if(cellWall.isEastOpen())
				return true;
			else
				return false;
		}
		
		public boolean canMoveSouth(int x, int y){
			CellWalls cellWall = getCell(x,y);
			
			if(cellWall.isSouthOpen())
				return true;
			else
				return false;
		}
		
		public boolean canMoveWest(int x, int y){
			CellWalls cellWall = getCell(x,y);
			
			if(cellWall.isWestOpen())
				return true;
			else
				return false;
		}
		
		
	
}

