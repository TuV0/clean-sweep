package depaul.se459.sensor;

/**
 * 
 * @author tuvo
 *
 */
public class SensorCell {
	    
	private int  cordX;
	private int  cordY;

	private String  northWall;
	private String  southWall;
	private String  eastWall;
	private String  westWall;
	
	
	private int  dirtUnit;

	private String  floorSurface;
	private boolean  charginStation;

	private CellWalls cellWall;

	public SensorCell(){		
		
	}
	public void setCellWalls(String  north, String south, String east, String west){		
		this.cellWall = new CellWalls(north,south,east,west);
	}
	public CellWalls getCellWalls(){
		return cellWall; 
	}
	public int getCordX(){
		return cordX;
	}
	public int getCordY(){
		return cordY;
	}	
	public void setCordXY(int cordX ,int cordY){
		this.cordX = cordX;
		this.cordY = cordY;
	}
	
	public String getfloorSurface(){
		return floorSurface;
	}
	public void setfloorSurface(String floorSurface){
		this.floorSurface = floorSurface;
	}
	
	public int getUnitOfDirt(){
		return dirtUnit;
	}
	public void setUnitOfDirt(int dirtUnit){
		this.dirtUnit = dirtUnit;
	}
	
	public String getFloorSuface(){
		return floorSurface;
	}
	public void setFloorSuface(String floorSurface){
		this.floorSurface = floorSurface;
	}
	
	public boolean getChargerStationLocation(){
		return charginStation;
	}
	public void setChargerStationLocation(String charginStation){
		
		if(charginStation.equals("no"))
			this.charginStation = false;
		else
			this.charginStation = true;
	}

}
