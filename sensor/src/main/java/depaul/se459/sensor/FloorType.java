package depaul.se459.sensor;

/**
 * This is the sensor which detects floor type
 * 
 * @author Mary
 */
public enum FloorType {
	HIGH_PILE, LOW_PILE, bare
}
