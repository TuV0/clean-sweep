package depaul.se459.sensor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * @author tuvo
 *
 */

public class SensorData {
	private List<SensorCell> cellList = new ArrayList<SensorCell>();;
	public SensorData() {
		
	}

    public List<SensorCell> getCellList() {
        return cellList;
    }
	
	public  List<SensorCell> readInJson(String fileJson) {     
		JSONParser parser = new JSONParser();

		try {			
			Object obj = parser.parse(new FileReader(fileJson));
			JSONObject jsonObject = (JSONObject) obj;		
					
			cellList= listOfCells(jsonObject);			
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return cellList;
	}
	
	private List<SensorCell> listOfCells (JSONObject jsonObject){
		
		JSONArray roomJsonArray = (JSONArray) jsonObject.get("cell");		
		@SuppressWarnings("unchecked")
		Iterator<JSONObject> iterator = roomJsonArray.iterator();
		
		while(iterator.hasNext()){				
			JSONObject cellJsonObj = iterator.next();
			
			String surface = (String)cellJsonObj.get("surface");

			JSONObject cellCord = (JSONObject)cellJsonObj.get("cord");
			int cellCordX = Integer.parseInt(cellCord.get("x").toString());
			int cellCordY = Integer.parseInt(cellCord.get("y").toString());

			
			int dirtUnit = Integer.parseInt(cellJsonObj.get("dirt").toString());
			
			
			JSONObject cellWalls = (JSONObject)cellJsonObj.get("CellWalls");
			String northWall = (String)cellWalls.get("NORTH");
			String southWall = (String)cellWalls.get("SOUTH");
			String eastWall = (String)cellWalls.get("EAST");
			String westWall = (String)cellWalls.get("WEST");

			String chargingStation = (String)cellJsonObj.get("chargingStation");


			SensorCell cell = SensorCellFact.createCell();
			cell.setFloorSuface(surface);
			cell.setCordXY(cellCordX, cellCordY);
			cell.setUnitOfDirt(dirtUnit);
			cell.setCellWalls(northWall, southWall, eastWall, westWall);
			cell.setChargerStationLocation(chargingStation);
			
			cellList.add(cell);
		}
						
	
		return cellList;
	}
	
		

}
