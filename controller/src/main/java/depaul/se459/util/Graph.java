package depaul.se459.util;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import depaul.se459.navigator.Cell;

public class Graph {

    /**
     * Stores a list of nodes in this Graph.Each cell is consider a node.
     */
    private ArrayList<Cell> nodes = new ArrayList<Cell>();

    /**
     * Creates a mapping from a node to its neighbours.
     */
    private Map<Cell, ArrayList<Cell>> map = new HashMap<Cell, ArrayList<Cell>>();

    /**
     * Constructs a graph.
     */
    public Graph() {
    }

    /**
     * Adds an edge between two nodes.
     *
     * @param source      the source node.
     * @param destination the destination node, to be connected from source. Requires:
     *                    source != null, destination != null.
     */
    public void addEdge(Cell source, Cell destination) {
        // Adds a new path.
        if (!map.containsKey(source)) {
            /*
            Stores a list of neighbours for a node.
            */
            ArrayList<Cell> neighbours = new ArrayList<Cell>();
            neighbours.add(destination);
            map.put(source, neighbours);
        } else {
            // Updates a path.
            ArrayList<Cell> oldList = map.get(source);

            int index = 0;
            while ((index != oldList.size()) && (!oldList.get(index).equals(destination))) {
                index++;
            }
            // If the destination is not already in the path, then
            // add it to the path.
            if (index == oldList.size()) {
                oldList.add(destination);
                map.put(source, oldList);
            }
        }
        storeNodes(source, destination);
    }

    /**
     * Stores the nodes in this Graph.
     */
    private void storeNodes(Cell source, Cell destination) {
        if (!source.equals(destination)) {
            if (!nodes.contains(destination)) {
                nodes.add(destination);
            }
        }
        if (!nodes.contains(source)) {
            nodes.add(source);
        }
    }

    /**
     * Returns the neighboursList for this node.
     *
     * @param node the node where its neighbours will be searched for. Requires:
     *             node must be present in this Graph and not null.
     * @return the neighboursList for this node.
     */
    public ArrayList<Cell> getNeighbours(Cell node) {
        ArrayList<Cell> neighboursList;
        Set<Cell> keys = map.keySet();
        for (Cell key : keys) {
            if (key.equals(node)) {
                neighboursList = map.get(key);
                return new ArrayList<Cell>(neighboursList);
            }
        }
        return new ArrayList<Cell>();
    }

    /**
     * Checks if the node is in this Graph.
     *
     * @return true if the node is in this Graph.
     */
    public boolean memberOf(Cell source) {
        return nodes.contains(source);
    }

    /**
     * Returns a string representation of this Graph, in
     * the form: node => [node 1, node 2, ... , node n], which means
     * that there is a path from node to node 1, node 2, ... , node n.
     *
     * @return a string representation of this Graph.
     */
    public String toString() {
        int counter = 0;
        String string = "cords";
        Set<Cell> keys = map.keySet();
        for (Cell key : keys) {
            if (counter == 0) {
                string = string + key.getLocationX()+key.getLocationY() + "--->" + map.get(key).toString();
            } else {
                string = string + "\n" + key.getLocationX() + "--->" + map.get(key).toString();
            }
            counter++;
        }
        return string;
    }
}
