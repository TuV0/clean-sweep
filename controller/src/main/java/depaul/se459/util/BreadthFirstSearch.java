package depaul.se459.util;

import java.util.ArrayDeque;
import java.util.ArrayList;

import depaul.se459.navigator.Cell;

public class BreadthFirstSearch {

    /**
     * The shortest path between two nodes in a graph.
     */
    private static ArrayList<Cell> shortestPath = new ArrayList<Cell>();

    /**
     * Finds the shortest path between two nodes (source and destination) in a graph.
     *
     * @param graph       The graph to be searched for the shortest path.
     * @param source      The source node of the graph specified by user.
     * @param destination The destination node of the graph specified by user.
     *
     * @return the shortest path stored as a list of nodes.
     * or null if a path is not found.
     * Requires: source != null, destination != null and must have a name (e.g.
     * cannot be an empty string).
     */
    public static ArrayList<Cell> breadthFirstSearch(Graph graph, Cell source,
    															  Cell destination) {
        shortestPath.clear();

        // A list that stores the path.
        ArrayList<Cell> path = new ArrayList<Cell>();

        // If the source is the same as destination, I'm done.
        if (source.equals(destination) && graph.memberOf(source)) {
            path.add(source);
            return path;
        }

        // A queue to store the visited nodes.
        ArrayDeque<Cell> queue = new ArrayDeque<Cell>();

        // A queue to store the visited nodes.
        ArrayDeque<Cell> visited = new ArrayDeque<Cell>();
        
        queue.offer(source);
        while (!queue.isEmpty()) {
        	Cell vertex = queue.poll();
            visited.offer(vertex);

            // get neighbors of current cell 
            ArrayList<Cell> neighboursList = graph.getNeighbours(vertex);
            int index = 0;
            int neighboursSize = neighboursList.size();
            while (index != neighboursSize) {
            	Cell neighbour = neighboursList.get(index);

                path.add(neighbour);
                path.add(vertex);

                if (neighbour.equals(destination)) {
                    return processPath(source, destination, path);
                } else {
                    if (!visited.contains(neighbour)) {
                        queue.offer(neighbour);
                    }
                }
                index++;
            }
        }
        return new ArrayList<Cell>();
    }

    /**
     * Adds the nodes involved in the shortest path.
     *
     * @param src         The source node.
     * @param destination The destination node.
     * @param path        The path that has nodes and their neighbours.
     * @return The shortest path.
     */
    private static ArrayList<Cell> processPath(Cell src, Cell destination,
                                                 ArrayList<Cell> path) {

        // Finds out where the destination node directly comes from.
        int index = path.indexOf(destination);
        Cell source = path.get(index + 1);

        // Adds the destination node to the shortestPath.
        shortestPath.add(0, destination);

        if (source.equals(src)) {
            // The original source node is found.
            shortestPath.add(0, src);
            return shortestPath;
        } else {
            // We find where the source node of the destination node
            // comes from.
            // We then set the source node to be the destination node.
            return processPath(src, source, path);
        }
    }
}
