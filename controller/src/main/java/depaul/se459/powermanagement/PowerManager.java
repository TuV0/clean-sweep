package depaul.se459.powermanagement;

import java.util.ArrayList;

import javax.swing.CellEditor;

import depaul.se459.controller.VacuumController;
import depaul.se459.navigator.Cell;
import depaul.se459.navigator.Layout;
import depaul.se459.state.VacuumState;
import depaul.se459.util.BreadthFirstSearch;
import depaul.se459.util.Graph;

public class PowerManager {
	private VacuumState vacuumState;
	private Layout layout;
	private Cell closestStation;
	private Battery battery;

	
	public PowerManager( VacuumController controller){
		vacuumState = VacuumState.getInstance();
		layout = controller.getLayout();
		battery = controller.getBattery();
	}
	
	public void charge(){	
		vacuumState.setBatteryLife(100);
		System.out.println("\nVACUUM IS FULLY CHARGED");
	}
	
	public void substractBattery(double amount){
		double battery = vacuumState.getBatteryLife();
		vacuumState.setBatteryLife(battery - amount);
	}
	
	/*public void detectChargingStations(int locationX, int locationY){
		int xs=locationX, ys=locationY; // Start coordinates

		// Check point (xs, ys)

		
		for (int d = 1; d<2; d++){
		    for (int i = 0; i < d + 1; i++){
		        int x = xs - d + i;
		        int y = ys - i;

		        //TODO add the function to sensor
		        //TODO Make the layout singleton
		        // Check point (x1, y1)
		        if (Sensor.getInstance().getCellFromSensor(x, y).getChargerStationLocation()) {
					Layout.getInstance().addChargingStation(x,y);
				}
		        

		        x = xs + d - i;
		        y = ys + i;

		        // Check point (x2, y2)
		        if (Sensor.getInstance().getCellFromSensor(x, y).getChargerStationLocation()) {
					Layout.getInstance().addChargingStation(x,y);
				}
		        
		    }


		    for (int i = 1; i < d; i++){
		        int x = xs - i;
		        int y = ys + d - i;

		        // Check point (x1, y1)
		        if (Sensor.getInstance().getCellFromSensor(x, y).getChargerStationLocation()) {
					Layout.getInstance().addChargingStation(x,y);
				}

		        x = xs + d - i;
		        y = ys - i;

		        // Check point (x2, y2)
		        if (Sensor.getInstance().getCellFromSensor(x, y).getChargerStationLocation()) {
					Layout.getInstance().addChargingStation(x,y);
				}
		    }
		}
	}*/
	
	private Double hasEnoughPower(Cell currentCell){
		//TODO Dijkstra
		ArrayList<Cell> chargingStations = layout.getChargingStationsList();
		ArrayList<Cell> pathToChargingStation = new ArrayList<Cell>();
		
		Double requiredBattery = Double.MAX_VALUE;
		Graph graph = new Graph();
		//ArrayList<Cell> currentLayout = layout.getLayoutList();
		
		graph = layout.getPath();
		
		for (Cell cell : chargingStations) {
			
			
			
			ArrayList<Cell> newPath = BreadthFirstSearch.breadthFirstSearch(graph, currentCell, cell);
			if (newPath == null) {
				newPath = new ArrayList<Cell>();
			}
			
			
			Double requiredBatteryForPath = 0.0;
			for (int i = 0; i < newPath.size()-1; i++) {
				requiredBatteryForPath += ((newPath.get(i).getCellWeight() + newPath.get(i+1).getCellWeight()) / 2);
			}
			if (requiredBatteryForPath < requiredBattery) {
				requiredBattery = requiredBatteryForPath;
				pathToChargingStation = newPath;
				closestStation = cell;
			}
		}
		
		
		return requiredBattery;
	}
	
	public boolean hasEnoughPowerToMove(Cell currentCell){
		
		if (vacuumState.getBatteryLife() >= (hasEnoughPower(currentCell) + (2 * (currentCell.getCellWeight() + 1)))) {
			return true;
		}
		return false;
		
	}
	
	public boolean hasEnoughPowerToVacuum(Cell currentCell){
		
		if (vacuumState.getBatteryLife() >= hasEnoughPower(currentCell) + (currentCell.getCellWeight())) {
			return true;
		}		
		return false;
		
	}

    public VacuumState getVacuumState() {
        return vacuumState;
    }
        

}

