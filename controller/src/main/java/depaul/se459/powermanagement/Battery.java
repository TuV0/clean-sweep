package depaul.se459.powermanagement;

import depaul.se459.controller.VacuumController;
import depaul.se459.state.VacuumState;

public class Battery {
	private double remainingBattery;
	private double maxBattery;
	
	public Battery(VacuumController controller){
		maxBattery = 100;
		remainingBattery = maxBattery;
	}
	public void substractBattery(double amount){
		if (!(remainingBattery - amount < 0)) {
			remainingBattery -= amount;
		}
		else {
			//TODO throw exception
		}
	}
	
	public void setRemainingBattery(double newRemaining){
		if (newRemaining >= 0 && newRemaining <= 100) {
			remainingBattery = newRemaining;
		}
		else{
			//TODO throw exception
		}		
	}
	
	public double getRemainingBattery(){
		return remainingBattery;
	}
	
	public double getBatteryLife (){
		return VacuumState.getInstance().getBatteryLife();		
	}
	
	public void setBatteryLife (double d){
		VacuumState.getInstance().setBatteryLife(d);	
	}

}
