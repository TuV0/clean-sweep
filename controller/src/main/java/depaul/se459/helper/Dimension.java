package depaul.se459.helper;
/**
 * 
 * @author tuvo
 *
 */
public class Dimension {
	
	private int width;
	private int height;
	
	
	public Dimension(){
	
	}
	
	public int getWith(){
		return width;
	}
	public void setWith(int width){
		this.width = width;
	}
	
	public int getHeight(){
		return height;
	}
	public void setHeight(int height){
		this.height = height;
	}
	public void setWidthHeight(int width, int height){
		this.width = width;
		this.height = height;
	}


}
