package depaul.se459.helper;
/**
 * 
 * @author tuvo
 *
 */
public class Coordinates {
	
	private int cordX;
	private int cordY;
	
	
	public Coordinates(){

	}
	
	public int getX(){
		return cordX;
	}
	
	public int getY(){
		return cordY;
	}
	public void setY(int y){
		this.cordY = y;
	}
	public void setX(int x){
		this.cordX = x;
	}
	public void setXY(int X, int Y){	
		cordX = X;
		cordY = Y;
	}
	
}
