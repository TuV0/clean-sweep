package depaul.se459.helper;

import depaul.se459.navigator.CellCondition;
/**
 * 
 * @author tuvo
 *
 */
public class CellWalls {
	
	private  String east;
	private  String west;
	private  String north;
	private  String south;
	
	    
    public CellWalls(String north,String south, String east,String west){
    	this.east = east;   
	    this.west = west;		
		this.north = north;
		this.south = south;
    }
    
	public CellCondition getEastWall(){  
		
        return CellCondition.valueOf(east);
    }
	
	public CellCondition getNorthWall(){    	
        return CellCondition.valueOf(north);
    }
	
	public CellCondition getWestWall(){    	
        return CellCondition.valueOf(west);
    }
	
	public CellCondition getSouthWall(){    	
        return CellCondition.valueOf(south);
    }
	
	public boolean isNorthOpen(){
		if(north.equals("OPEN"))
			return true;
		else
			return false;
	}
	public boolean isSouthOpen(){
		if(south.equals("OPEN"))
			return true;
		else
			return false;
	}
	public boolean isWestOpen(){
		if(west.equals("OPEN"))
			return true;
		else
			return false;
	}
	public boolean isEastOpen(){
		if(east.equals("OPEN"))
			return true;
		else
			return false;
	}
	

}
