package depaul.se459.state;

import java.util.ArrayList;

import depaul.se459.navigator.Cell;

public class VacuumState {
	
	private static VacuumState instance;
	private double  batteryState;
	private int dirtCapacityState;
	private boolean emptyMeIndicator;
	private Cell currentCell;
	private Cell previousCell;
	private ArrayList<Cell> visitedList;


	public VacuumState(){
		this.batteryState = 100;
		this.dirtCapacityState = 0;
		this.emptyMeIndicator = false;
		
		visitedList = new ArrayList<Cell>();
		
	}
	public ArrayList<Cell> getVisitedList(){
		return visitedList;
	}
		
	public static VacuumState getInstance() {
		if (instance == null) {
			instance = new VacuumState();
		}
		return instance;
	}
	
	public double getBatteryLife (){
		return batteryState;		
	}
	public void setBatteryLife (double d){
		batteryState =d;		
	}
	
	public int getDirtCapacityState (){
		return dirtCapacityState;
	}
	
	public void resetDirtCapacityState(int dirtUnit){
		this.dirtCapacityState = dirtUnit;
		setEmptyIndicatorState(false);
	}
	
	public boolean getEmptyIndicatorState(){		
		return emptyMeIndicator;
	}
	public void setEmptyIndicatorState (boolean emptyState){		
		this.emptyMeIndicator = emptyState;
	}
	
	public Cell getCurrentCell(){
		return currentCell;
	}
	
	public void setCurrentCell(Cell currentCell){
		this.currentCell = currentCell;
				
	}
	
	public Cell getPreviousCell(){
		return previousCell;
	}
	
	public void setPreviousCell(Cell previousCell){
		this.previousCell = previousCell; 
	}
	
}
