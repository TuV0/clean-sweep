package depaul.se459.clean_dirt;

/**
 * This is the sensor which detects floor type
 * 
 * @author Mary
 */
public interface FloorSurface {
	public enum FloorType {
		HIGH_PILE, LOW_PILE, BARE_FLOOR
	}

	public FloorType getFloorType();

}
