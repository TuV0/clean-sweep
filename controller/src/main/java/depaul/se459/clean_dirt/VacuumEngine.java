package depaul.se459.clean_dirt;

import java.util.ArrayList;

import depaul.se459.controller.VacuumController;
import depaul.se459.navigator.Cell;
import depaul.se459.navigator.Layout;
import depaul.se459.powermanagement.PowerManager;
import depaul.se459.sensor.Sensor;
import depaul.se459.state.VacuumState;

public class VacuumEngine {
	private final int MAX_DIRT_CAPACITY = 50;
	private int currentDirtAmount;

	private PowerManager powerManager;
	private VacuumState vacuumState;
	private VacuumController controller;

	private Sensor sensor;
	private Layout layout;

	public VacuumEngine(VacuumController controller) {

		this.controller = controller;
		powerManager = controller.getPowerManager();
		layout = controller.getLayout();

		vacuumState = VacuumState.getInstance();
		sensor = Sensor.getInstance();

	}

	public void cleanIfDirty(Cell currentCell) {
                
		while (isDirty(currentCell)) {
			if (powerManager.hasEnoughPowerToVacuum(currentCell)) {
				extractDirt(currentCell); 
			} else {
				ArrayList<Cell> pathToStation = layout.closestPathChrgnStation(currentCell);
				//controller.getNavigator().print();
				controller.getNavigator().moveToChargeStation(pathToStation);
			}
			                   
		}

	}

	public boolean isDirty(Cell cell) {
		return sensor.isDirty(cell.getLocationX(), cell.getLocationY());
	}

	private void extractDirt(Cell currentCell) {

		//remove dirt from sensor cell
		sensor.extractDirt(currentCell.getLocationX(), currentCell.getLocationY(), 1);

		//extract batter for dirty cleaning
		extractBattery(currentCell.getCellWeight());
		vacuumState.resetDirtCapacityState(++currentDirtAmount);

		if (isAtDirtCapacity()) {
                        
			ArrayList<Cell> pathToStation = layout.closestPathChrgnStation(currentCell);
			//controller.getNavigator().print();
			controller.getNavigator().moveToChargeStation(pathToStation);
		}
		
		
	}

	public int getDirtCapacity() {
		return currentDirtAmount;
	}

	public boolean isAtDirtCapacity() {
		if (currentDirtAmount == MAX_DIRT_CAPACITY) {
			vacuumState.setEmptyIndicatorState(true);
			return true;
		}
		else{
			vacuumState.setEmptyIndicatorState(false);
			return false;
		}
	}

	private void extractBattery(double amount) {
		powerManager.substractBattery(amount);
	}

	public int getCurrentDirtAmount (){
		return VacuumState.getInstance().getDirtCapacityState();
	}
	
	public void setCurrentDirtAmount(int dirtUnit){
		VacuumState.getInstance().resetDirtCapacityState(dirtUnit);
		currentDirtAmount =0;
	}
	
	public boolean getEmptyIndicator(){		
		return vacuumState.getEmptyIndicatorState();
	}
	public void setEmptyIndicatorS(boolean emptyState){		
		vacuumState.setEmptyIndicatorState(emptyState);
	}

    public PowerManager getPowerManager() {
        return powerManager;
    }

    public Sensor getSensor() {
        return sensor;
    }
    
    
        
}
