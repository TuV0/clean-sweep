package depaul.se459.controller;

import org.apache.log4j.Logger;

import depaul.se459.clean_dirt.VacuumEngine;
import depaul.se459.navigator.Layout;
import depaul.se459.navigator.Navigate;
import depaul.se459.powermanagement.Battery;
import depaul.se459.powermanagement.PowerManager;
/**
 * 
 * @author tuvo
 *
 */
public class VacuumController {
	final static Logger logger = Logger.getLogger(VacuumController.class);
	private Navigate nav;
	private PowerManager powerMngr;
	private Battery battery;
	private VacuumEngine vacuumEng;
	private Layout layout;

	public VacuumController(){
		initializeAll();
		
                /*Sarica: I am commenting nav.move() call as I need to initialize VacuumController for unit tests 
                so that test data is set. And I don't want VacuumController to move right after it is initialized
                as it will reset the data to data of clean room. 
                I have moved nav.move() call to App.java*/
                
                //nav.move();
		
	}
	private void initializeAll(){
		try{

			layout = new Layout(this);
			powerMngr = new PowerManager(this);
			battery = new Battery(this);
			vacuumEng = new VacuumEngine(this);
			nav = new Navigate(this);	
					
		}catch(Exception e){
			logger.fatal("somethign went wrong, not initialing");
		}
		
	}
	
	public Navigate getNavigator(){
		return nav;
	}
	
	public PowerManager getPowerManager(){
		return powerMngr;
	}
	
	public Battery getBattery(){
		return battery;
	}
	public VacuumEngine getVacuumEngine(){
		return vacuumEng;
	}
	public Layout getLayout(){
		return layout;
	}

	

}

