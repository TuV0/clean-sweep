package depaul.se459.navigator;

public class ControlSystemLayout {
    
    static final String EAST_DIRECTION="E";
    static final String WEST_DIRECTION="W";
    static final String NORTH_DIRECTION="N";
    static final String SOUTH_DIRECTION="S";
    
    int vcX;        //x coordinate 
    int vcY;        //y coordinate

    /*void sweep(){
        while(canMoveTo(EAST_DIRECTION)){
            moveEast();
        }
        System.out.println("Reached east end");
    }
            
    boolean moveEast(){
        if(canMoveTo(EAST_DIRECTION)){
            vcX++;
            System.out.println("Moving to ["+ vcX+"]["+vcY+"]");
            return true;
        }
        return false;
    }
    
    boolean moveWest(){
        if(canMoveTo(WEST_DIRECTION)){
            vcX--;
            System.out.println("Moving to ["+ vcX+"]["+vcY+"]");
            return true;        
        }
        return false;
    }
    
    boolean moveNorth(){
        if(canMoveTo(NORTH_DIRECTION)){
            vcY++;
            System.out.println("Moving to ["+ vcX+"]["+vcY+"]");
            return true;
        }
        return false;
    }
    
    boolean moveSouth(){
        if(canMoveTo(SOUTH_DIRECTION)){
            vcY++;
            System.out.println("Moving to ["+ vcX+"]["+vcY+"]");
            return true;
        }
        return false;
    }
    
    boolean canMoveTo(String direction){
        int cellToCheckX=vcX, cellToCheckY=vcY;
        
        //identifying which is the next cell to check
        if(direction.equals(EAST_DIRECTION)){
            cellToCheckX++;
        }else if(direction.equals(WEST_DIRECTION)){
            cellToCheckX--;
        }else if(direction.equals(NORTH_DIRECTION)){
            cellToCheckY++;
        }else if (direction.equals(SOUTH_DIRECTION)){
            cellToCheckY--;
        }
        
        //checking boundaries
        if(cellToCheckX<0 || cellToCheckY<0 || cellToCheckX>9 || cellToCheckY>9){
            return false;
        }
        
        //check obstacles
        if(layoutArray[cellToCheckX][cellToCheckY]=-1){ 
            // layoutArray[x][y]=-1 represents wall/obstacle/closed door
            return false;
        }
        
        return true;
    }
    
    public static void main(String [] args){
        ControlSystemLayout obj=new ControlSystemLayout();
        obj.sweep();
    }*/
}
