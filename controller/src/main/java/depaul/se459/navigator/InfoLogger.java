package depaul.se459.navigator;
import java.time.LocalDateTime;
import java.util.HashMap;


public class InfoLogger {
	
	private HashMap<LocalDateTime, String> movementLog;
	
	public InfoLogger(){
		movementLog = new HashMap<LocalDateTime, String>();
	}
	
	void logMovement(int currentX,int currentY,int newX,int newY){
		movementLog.put(LocalDateTime.now(), "From ["+currentX+"]["+currentY+"] to ["+newX+"]["+newY+"]");
	}

}
