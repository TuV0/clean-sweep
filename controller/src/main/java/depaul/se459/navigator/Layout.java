package depaul.se459.navigator;

import java.util.ArrayList;
import java.util.Iterator;

import depaul.se459.controller.VacuumController;
import depaul.se459.state.VacuumState;
import depaul.se459.util.BreadthFirstSearch;
import depaul.se459.util.Graph;

public class Layout {

	private ArrayList<Cell> chargingStnList;
	private ArrayList<Cell> layoutList;
	private Graph path;
	private Cell currentCell;
	private Cell previousCell;

	@SuppressWarnings("unused")
	private VacuumController controller;
	
	public Layout(VacuumController controller) {
		
		this.controller = controller;
		
		layoutList = new ArrayList<Cell>();
		path = new Graph();
	}


	public ArrayList<Cell> getLayoutList(){
		return layoutList;
		
	}
	
	public Cell getCurrentCell(){
		currentCell = VacuumState.getInstance().getCurrentCell();

		return currentCell;
	}
	
	public void setCurrentCell(Cell crntCell){
		VacuumState.getInstance().setCurrentCell(crntCell); 
	}
	
	public Cell getPreviousCell(){
		return previousCell;
	}
	
	public void setPreviousCell(Cell previousCell){
		this.previousCell = previousCell; 
	}
	
	public Graph getPath (){
		return path;
	}

	public ArrayList<Cell> getChargingStationsList() {
		// TODO Auto-generated method stub
		chargingStnList = new ArrayList<Cell>();
		
		Iterator<Cell> list = layoutList.iterator();

		while (list.hasNext()) {
			Cell chrgnStationCell = list.next();
			if(chrgnStationCell.isCharginStation()){
				chargingStnList.add(chrgnStationCell);
			}
		}
		return chargingStnList;
	}
	
	public ArrayList<Cell> closestPathChrgnStation(Cell currentCell){
		
		ArrayList<Cell> closestStationList= getChargingStationsList();
		Cell firstCharger = closestStationList.get(0);
		
		ArrayList<Cell> pathToChrgnStation = BreadthFirstSearch.breadthFirstSearch(path, currentCell, firstCharger);
		ArrayList<Cell> pathTemp = new ArrayList<Cell>(); 

		//return the shortest path amongst all charging station listed
		for(Cell chrgnStation : closestStationList){
			pathTemp = BreadthFirstSearch.breadthFirstSearch(path, currentCell, chrgnStation);
			
			if( pathTemp.size() < pathToChrgnStation.size()){
				pathToChrgnStation = pathTemp;
			}
			
		}
		return pathToChrgnStation;
	}

	public ArrayList<Cell> getCurrentLayout() {
		// TODO Auto-generated method stub
		return null;
	}

}
