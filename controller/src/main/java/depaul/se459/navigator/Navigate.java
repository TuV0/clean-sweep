package depaul.se459.navigator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.log4j.Logger;
import depaul.se459.clean_dirt.VacuumEngine;
import depaul.se459.controller.VacuumController;
import depaul.se459.powermanagement.Battery;
import depaul.se459.sensor.Sensor;
import depaul.se459.state.VacuumState;
import depaul.se459.util.BreadthFirstSearch;
import depaul.se459.util.Graph;
/**
 * This class is the core of the system, it makes decisions base of the status of Battery and Dirt cleaning classes
 * @author tuvo
 *
 */
public class Navigate {

	final static Logger logger = Logger.getLogger(Navigate.class);
	private Deque<Cell> stack;
	private ArrayList<Cell> visitedList;
	private Cell crntCell;
	private Cell destinationCell;
	private Cell previousCell;
	private Graph graphPath;
	private VacuumController controller;
	private Sensor sensor;
	private Layout layout;
	private Battery battery;
	private VacuumEngine vacuumEngine;



	public Navigate(VacuumController controller) {
		stack = new ArrayDeque<Cell>();
		
		this.controller = controller;
		this.sensor = Sensor.getInstance();
		 
		layout = controller.getLayout();
		battery = controller.getBattery();
		vacuumEngine = controller.getVacuumEngine();
		
		graphPath = layout.getPath();
		visitedList =layout.getLayoutList();
		crntCell = layout.getCurrentCell();
		
		startAtCell(0,0);
	}
	
	public void startAtCell(int x, int y) {
		// set coordinates of current cell to initialize
		crntCell = createLayoutCell(x, y);
		markVisited(crntCell);
	}

	public void move() {

		//check all 4 direction north,east,south,west 
		while (!stack.isEmpty()) {
			
			int curntX = crntCell.getLocationX();
			int curntY = crntCell.getLocationY();

			if (controller.getPowerManager().hasEnoughPowerToMove(crntCell)) {
				if (sensor.canMoveNorth(curntX, curntY)
						&& !isVisted(curntX, curntY - 1)) {

					setAsDestinationCell(curntX, curntY - 1);
					isClearToMove();

				} else if (sensor.canMoveEast(curntX, curntY)
						&& !isVisted(curntX + 1, curntY)) {
					setAsDestinationCell(curntX + 1, curntY);
					isClearToMove();

				} else if (sensor.canMoveSouth(curntX, curntY)
						&& !isVisted(curntX, curntY + 1)) {
					setAsDestinationCell(curntX, curntY + 1);
					isClearToMove();

				} else if (sensor.canMoveWest(curntX, curntY)
						&& !isVisted(curntX - 1, curntY)) {
					setAsDestinationCell(curntX - 1, curntY);
					isClearToMove();

				} else {
					//remove current position from stack
					try {
						stack.pop();
						crntCell = stack.getFirst();
					} catch (NoSuchElementException e) {
						logger.warn("NoSuchElement warning: " + e);
					}
				}
			} 
			else {
				
				ArrayList<Cell> pathToStation = layout.closestPathChrgnStation(crntCell);
				moveToChargeStation(pathToStation);
			}
		}
			
		moveBackToStationWhenFinished();

		System.out.println();
		System.out.println("FINISHED CLEANING");
		System.out.println();
	}

	// check single cell if it has been visited
	public boolean isVisted(int x, int y) {
		boolean isVistedCell = false;
		Iterator<Cell> visited = visitedList.iterator();	
		
		while (visited.hasNext()) {
			Cell c = visited.next();
			if (c.getLocationX() == x && c.getLocationY() == y)
				isVistedCell = true;
		}
		return isVistedCell;
	}
	public void setAsDestinationCell(int destX, int destY){
		destinationCell = createDestinCell(destX,destY);
	}

	public Cell createLayoutCell(int cordX, int cordY) {
		
		boolean isDirty = sensor.isDirty(cordX, cordY);
		boolean ischargingStat = sensor.isCharginStation(cordX, cordY);
		String getFloorType = sensor.getFloorType(cordX, cordY);	
		
		crntCell = new Cell(cordX, cordY, CellCondition.OPEN, getFloorType, isDirty, ischargingStat);
		layout.setCurrentCell(crntCell);
		
		return crntCell;
	}

	// mark as visited and push to queue
	public void markVisited(Cell dest) {				
		visitedList.add(dest);
		stack.push(dest);
		
		//set destination to previous
		layout.setPreviousCell(dest);

	}

	public Cell createDestinCell(int x, int y) {
		Cell destination = createLayoutCell(x, y);
		return destination;
	}

	//forward and back path
	public void storeToGraphPath(Cell prevCell, Cell destination) {
		graphPath.addEdge(prevCell, destination);
		graphPath.addEdge(destination, prevCell);
	}
	
	public void moveToChargeStation(ArrayList<Cell> path){
		moveToDestination(path);
		if (!vacuumEngine.getEmptyIndicator()) {
			controller.getPowerManager().charge();
		}
		while(vacuumEngine.getEmptyIndicator()){
			
			printChrgnStationStatus();
			String userInput = "y";			
			if(userInput.contains("y")){
				vacuumEngine.setCurrentDirtAmount(0);;
			}			
		}
		moveToUnfinishedCell(layout.getCurrentCell(), stack.peek());			
	}
	
	public void moveToUnfinishedCell(Cell current, Cell destination){

		ArrayList<Cell> toUnfishedCell = new ArrayList<Cell>();
		toUnfishedCell = BreadthFirstSearch.breadthFirstSearch(graphPath, current, destination);
		moveToDestination(toUnfishedCell);
	}

	// TODO path to move to destination
	public void moveToDestination(ArrayList<Cell> path) {
		Iterator<Cell> pathIterator = path.iterator();
		
		while (pathIterator.hasNext()) {
			
			Cell curntCell = pathIterator.next();
			
			//decrement battery for each move
			controller.getPowerManager().substractBattery((layout.getCurrentCell().getCellWeight() + curntCell.getCellWeight())/2);
			
			//set currentCell to vacuum state
			layout.setCurrentCell(curntCell);
						
			System.out.println("");
			System.out.println("MOVING TO... Cell: " +curntCell.getLocationX() + "," + curntCell.getLocationY()  +
							   " dirtCap: "+ vacuumEngine.getCurrentDirtAmount() + " Remaining Battery: " + battery.getBatteryLife() + " Floor: " + curntCell.getFloorType());		
		}
	}
	public void moveBackToStationWhenFinished(){		
		System.out.println("RETURNING TO STATION");
		Cell getCrntCell = layout.getCurrentCell();
		ArrayList<Cell> pathToStation = layout.closestPathChrgnStation(getCrntCell);
		moveToChargeStation(pathToStation);
	}

	public void isClearToMove() {
		
		boolean isAtDirtCapacity = VacuumState.getInstance().getEmptyIndicatorState();
		
		if(!isAtDirtCapacity){		

			previousCell = layout.getPreviousCell();
			
			//store path to destination and return
			storeToGraphPath(previousCell, destinationCell);
			
			//clean all dirty before moving to next  cell
			VacuumEngine vac = controller.getVacuumEngine();
			vac.cleanIfDirty(crntCell);		
			
			//once moved decrement battery for traversing one cell to another
			
			double tranverseBatteryCost = (previousCell.getCellWeight()+destinationCell.getCellWeight())/2;
			controller.getPowerManager().substractBattery(tranverseBatteryCost);			
			print();
									
		}
		//done visiting
		markVisited(destinationCell);				
	}

	public void print() {
		
		Cell prevCell = layout.getPreviousCell();
		int prevX = prevCell.getLocationX();
		int prevY = prevCell.getLocationY();
		int crntX = crntCell.getLocationX();
		int crntY = crntCell.getLocationY();
				
		System.out.println(" || Current: " + prevX + "," + prevY+ " Destination: " + crntX + "," + crntY + 
						  "   || Has Dirt: " + crntCell.isDirty() + "	" +
						  	" || Floor: " + crntCell.getFloorType() + " " + " 	" +
						  	" || Battery:" + battery.getBatteryLife() + 
						 	" || DirtCapacity: " + vacuumEngine.getCurrentDirtAmount()); 			
	}
	public void printChrgnStationStatus(){
		System.out.println();
		System.out.println("**EMPTY INDICATOR... : " + vacuumEngine.getEmptyIndicator() + "**");
		System.out.println("WOULD YOU LIKE TO EMPTY CANNISTER? Y or N");
		System.out.println("User enter: y");
		
	}

    public ArrayList<Cell> getVisitedList() {
        return visitedList;
    }

    public Cell getDestinationCell() {
        return destinationCell;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public Graph getGraphPath() {
        return graphPath;
    }

    public Layout getLayout() {
        return layout;
    }

    
}

