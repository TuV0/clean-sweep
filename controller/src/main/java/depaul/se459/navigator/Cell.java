package depaul.se459.navigator;

import depaul.se459.sensor.FloorType;

public class Cell {

	private int locationX;
	private int locationY;
	private CellCondition condition;
	private FloorType floorType;
	private boolean dirty;
	private boolean chargeStation;
	private int cellWeight;

	public Cell(){
		
	}
	
	//FOR TESTING
	public Cell(int x, int y){
		this.locationX =x;
		this.locationY =y;
	}
	public Cell(int locationXIn, int locationYIn, CellCondition cellCondtn, String floorType, boolean isDirty, boolean isCharger) {
		locationX = locationXIn;
		locationY = locationYIn;
		this.floorType = FloorType.valueOf(floorType);
		this.condition = cellCondtn;
		this.dirty = isDirty;
		this.chargeStation = isCharger;

	}
	
	public double getCellWeight(){
		
		if(floorType == FloorType.bare)
			cellWeight = 1;		
		else if(floorType == FloorType.LOW_PILE)
			cellWeight =2;
		else if(floorType == FloorType.HIGH_PILE)
			cellWeight =3;

		return cellWeight;
	}
	public boolean isCharginStation() {
		
		return chargeStation;
	}

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean isDirty) {
		dirty = isDirty;
	}

	public void setCellCondition(CellCondition conditionIn) {
		condition = conditionIn;
	}

	public CellCondition getCellCondition() {
		return condition;
	}

	public int getLocationX() {
		return locationX;
	}

	public void setLocationX(int locationX) {
		this.locationX = locationX;
	}

	public int getLocationY() {
		return locationY;
	}

	public void setLocationY(int locationY) {
		this.locationY = locationY;
	}

	public CellCondition getCondition() {
		return condition;
	}

	public void setCondition(CellCondition condition) {
		this.condition = condition;
	}
	
	public FloorType getFloorType(){
		return floorType;
	}

}
