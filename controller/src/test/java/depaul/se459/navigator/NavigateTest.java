/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package depaul.se459.navigator;

import depaul.se459.controller.VacuumController;
import depaul.se459.state.VacuumState;
import depaul.se459.util.BreadthFirstSearch;
import depaul.se459.util.Graph;
import java.util.ArrayList;
import java.util.Map;
import junit.framework.TestCase;

/**
 *
 * @author Sarica
 */
public class NavigateTest extends TestCase {
    VacuumController vc;
    Navigate instance;
    
    public NavigateTest(String name) {
        super(name);
    }
    
    public void setUp() {
        vc=new VacuumController();
        instance = new Navigate(vc);                
    }
    
    public void tearDown() {
        vc=null;
        instance=null;
    }

    /**
     * Test of startAtCell method, of class Navigate.
     */
    
   /* public void testStartAtCell() {
        System.out.println("startAtCell");
        int x = 0;
        int y = 0;
        boolean result=false;
        
        instance.startAtCell(x, y);
        System.out.println("VisitedListSize: "+instance.getVisitedList().size());
        
        for (int i=0;i<instance.getVisitedList().size();i++){
            int xpos=((Cell)instance.getVisitedList().get(i)).getLocationX();
            int ypos=((Cell)instance.getVisitedList().get(i)).getLocationX();
            System.out.println(xpos+","+ypos);
            
            if(xpos==x && ypos==y) {
                result=true;
                break;
            }
        }
        
        assertTrue(result==true);
    }*/
 
    /**
     * Test of move method, of class Navigate.
     */
   /*
    public void testMove() {
        System.out.println("move");
        Navigate instance = null;
        instance.move();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    
    /**
     * Test of isVisted method, of class Navigate.
     */
    public void testIsVisted() {
        System.out.println("isVisted");
        instance.getVisitedList().add(new Cell(0,0));
        
        boolean result = instance.isVisted(0,0);
        assertTrue(result==true);
        
        result = instance.isVisted(2,0);
        assertTrue(result==false);
    }
    
    /**
     * Test of setAsDestinationCell method, of class Navigate.
     */
    public void testSetAsDestinationCell() {
        System.out.println("setAsDestinationCell");
        int destX = 0;
        int destY = 0;
        boolean result=false;
        
        instance.setAsDestinationCell(destX, destY);
        
        if(instance.getDestinationCell().getLocationX()==destX &&
                instance.getDestinationCell().getLocationY()==destY){
            result=true;
        }
        assertTrue(result==true);
    }
    
    /**
     * Test of createLayoutCell method, of class Navigate.
     */
    public void testCreateLayoutCell() {
        System.out.println("createLayoutCell");
        int cordX = 0;
        int cordY = 0;
        boolean result=false;
        
        
        Cell cell = instance.createLayoutCell(cordX, cordY);
       
        if(cell.getLocationX()==cordX &&
                cell.getLocationY()==cordY &&
                instance.getSensor().isDirty(cordX, cordY)==cell.isDirty() &&
                instance.getSensor().isCharginStation(cordX, cordY)==cell.isCharginStation() &&
                instance.getSensor().getFloorType(cordX, cordY).equals(cell.getFloorType().toString())){
            result=true;
        }
        
         assertTrue(result==true);
    }
    
    /**
     * Test of markVisited method, of class Navigate.
     */
    public void testMarkVisited() {
        System.out.println("markVisited");
        Cell dest = new Cell(1,0);
        
        instance.markVisited(dest);
        assertTrue(instance.isVisted(1, 0)==true);
    }
    
    /**
     * Test of createDestinCell method, of class Navigate.
     */
    
    public void testCreateDestinCell() {
        System.out.println("createDestinCell");
        int cordX = 0;
        int cordY = 0;
        boolean result=false;
        
        
        Cell cell = instance.createDestinCell(cordX, cordY);
       
        if(cell.getLocationX()==cordX &&
                cell.getLocationY()==cordY &&
                instance.getSensor().isDirty(cordX, cordY)==cell.isDirty() &&
                instance.getSensor().isCharginStation(cordX, cordY)==cell.isCharginStation() &&
                instance.getSensor().getFloorType(cordX, cordY).equals(cell.getFloorType().toString())){
            result=true;
        }
        
        assertTrue(result==true);
    }
    
    /**
     * Test of storeToGraphPath method, of class Navigate.
     */
    public void testStoreToGraphPath() {
        System.out.println("storeToGraphPath");
        Cell prevCell = new Cell(1,0);
        Cell destination = new Cell(2,0);
        Map<Cell, ArrayList<Cell>> map;
                
        instance.storeToGraphPath(prevCell, destination);
        
        ArrayList<Cell> sourceNeighbours=instance.getGraphPath().getNeighbours(prevCell);
        ArrayList<Cell> destNeighbours=instance.getGraphPath().getNeighbours(destination);
        
        int index = 0;
        while ((index != sourceNeighbours.size()) && (!sourceNeighbours.get(index).equals(destination))) {
                index++;
        }
        assertTrue(index != sourceNeighbours.size());
        
        index = 0;
        while ((index != destNeighbours.size()) && (!destNeighbours.get(index).equals(prevCell))) {
                index++;
        }
        assertTrue(index != destNeighbours.size());
        
        assertTrue(instance.getGraphPath().memberOf(prevCell));
        assertTrue(instance.getGraphPath().memberOf(destination));        
    }
    
    /**
     * Test of moveToChargeStation method, of class Navigate.
     */
    /*public void testMoveToChargeStation() {
        System.out.println("moveToChargeStation");
        ArrayList<Cell> path = null;
        Navigate instance = null;
        instance.moveToChargeStation(path);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    
    /**
     * Test of moveToUnfinishedCell method, of class Navigate.
     */
    public void testMoveToUnfinishedCell() {
        System.out.println("moveToUnfinishedCell");
        Cell current = new Cell(1,0);
        Cell int1 = new Cell(2,0);
        Cell int2 = new Cell(3,0);
        Cell destination = new Cell(4,0);
        
        instance.storeToGraphPath(current, int1);
        instance.storeToGraphPath(int1, int2);
        instance.storeToGraphPath(int2, destination);
        
        instance.moveToUnfinishedCell(current, destination);
        assertTrue(instance.getLayout().getCurrentCell()==destination);
    }
    
    /**
     * Test of moveToDestination method, of class Navigate.
     */
    public void testMoveToDestination() {
        System.out.println("moveToDestination");
        Cell current = new Cell(1,0);
        Cell int1 = new Cell(2,0);
        Cell int2 = new Cell(3,0);
        Cell destination = new Cell(4,0);
        Graph graphPath=new Graph();
        
        graphPath.addEdge(current, int1);
	graphPath.addEdge(int1, current);
                
        graphPath.addEdge(int1, int2);
	graphPath.addEdge(int2, int1);
                
        graphPath.addEdge(int2, destination);
	graphPath.addEdge(destination, int2);
        
        ArrayList<Cell> path=BreadthFirstSearch.breadthFirstSearch(graphPath, current, destination);
	
        instance.moveToDestination(path);
        assertTrue(instance.getLayout().getCurrentCell()==destination);
    }
    
    /**
     * Test of isClearToMove method, of class Navigate.
     */
   /* public void testIsClearToMove() {
        System.out.println("isClearToMove");
        Cell cell=new Cell(1,0);
        Cell dest=new Cell(2,0);
        
        instance.getLayout().setCurrentCell(cell);
        instance.getLayout().setPreviousCell(cell);
        instance.setAsDestinationCell(2, 0);
        
        instance.isClearToMove();
        assertTrue(instance.isVisted(2, 0)==true);
        
        System.out.println("getEmptyIndicatorState(): "+VacuumState.getInstance().getEmptyIndicatorState());
        
        ArrayList<Cell> sourceNeighbours=instance.getGraphPath().getNeighbours(cell);
        ArrayList<Cell> destNeighbours=instance.getGraphPath().getNeighbours(dest);
        
        int index = 0;
        while ((index != sourceNeighbours.size()) && (!sourceNeighbours.get(index).equals(dest))) {
            System.out.println("index: "+index+" sourceNeighbours.get(index): "+sourceNeighbours.get(index).getLocationX()+","+sourceNeighbours.get(index).getLocationY());
            index++;
        }
        System.out.println("index: "+index+" size: "+sourceNeighbours.size());
        assertTrue(index != sourceNeighbours.size());
        
        index = 0;
        while ((index != destNeighbours.size()) && (!destNeighbours.get(index).equals(cell))) {
             System.out.println("index: "+index+" destNeighbours.get(index): "+destNeighbours.get(index).getLocationX()+","+destNeighbours.get(index).getLocationY());
            index++;
        }
        assertTrue(index != destNeighbours.size());
        
        assertTrue(instance.getGraphPath().memberOf(cell));
        assertTrue(instance.getGraphPath().memberOf(dest));
        
        System.out.println("isDirty:"+instance.getSensor().isDirty(1, 0));
        assertTrue(instance.getSensor().isDirty(1, 0)== false);
    }
    */
}
