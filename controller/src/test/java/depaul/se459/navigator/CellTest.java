/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package depaul.se459.navigator;

import depaul.se459.sensor.FloorType;
import junit.framework.TestCase;

/**
 *
 * @author Sarica
 */
public class CellTest extends TestCase {
    
     Cell instance;
    
    public CellTest(String name) {
        super(name);
    }
    
    /**
     * Test of getCellWeight method, of class Cell.
     */
    public void testGetCellWeight() {
        System.out.println("getCellWeight");
        
        Cell cell1 = new Cell(1,0, CellCondition.OPEN, "bare", true, false);
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell3 = new Cell(3,0, CellCondition.OPEN, "HIGH_PILE", true, false);
                
        assertTrue(cell1.getCellWeight()==1);
        assertTrue(cell2.getCellWeight()==2);
        assertTrue(cell3.getCellWeight()==3);       
    }    
    
    /**
     * Test of isCharginStation method, of class Cell.
     */
    public void testIsCharginStation() {
        System.out.println("isCharginStation");
        Cell cell1 = new Cell(1,0, CellCondition.OPEN, "bare", true, true);
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", true, false);
        
        assertTrue(cell1.isCharginStation()==true);
        assertTrue(cell2.isCharginStation()==false);
    }
    
    /**
     * Test of isDirty method, of class Cell.
     */
    public void testIsDirty() {
        System.out.println("isDirty");
        Cell cell1 = new Cell(1,0, CellCondition.OPEN, "bare", true, false);
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", false, false);
        
        assertTrue(cell1.isDirty()==true);
        assertTrue(cell2.isDirty()==false);
    }

    /**
     * Test of setDirty method, of class Cell.
     */    
    public void testSetDirty() {
        System.out.println("setDirty");
       Cell cell1 = new Cell(1,0);
       
        cell1.setDirty(true);
        assertTrue(cell1.isDirty()==true);
        
        cell1.setDirty(false);
        assertTrue(cell1.isDirty()==false);        
    }
    
    /**
     * Test of setCellCondition method, of class Cell.
     */
    public void testSetCellCondition() {
        System.out.println("setCellCondition");
        Cell cell1 = new Cell(1,0);
       
        cell1.setCellCondition(CellCondition.OBSTACLE);
        assertTrue(cell1.getCellCondition()==CellCondition.OBSTACLE);
        
        cell1.setCellCondition(CellCondition.OPEN);
        assertTrue(cell1.getCellCondition()==CellCondition.OPEN);
        
        cell1.setCellCondition(CellCondition.STAIRS);
        assertTrue(cell1.getCellCondition()==CellCondition.STAIRS);
        
        cell1.setCellCondition(CellCondition.UNKNOWN);
        assertTrue(cell1.getCellCondition()==CellCondition.UNKNOWN);
    }
    
    /**
     * Test of getCellCondition method, of class Cell.
     */
    public void testGetCellCondition() {
        System.out.println("getCellCondition");
        
        Cell cell1 = new Cell(1,0, CellCondition.OBSTACLE, "bare", true, false);
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell3 = new Cell(3,0, CellCondition.STAIRS, "HIGH_PILE", true, false);
        Cell cell4 = new Cell(4,0, CellCondition.UNKNOWN, "HIGH_PILE", true, false);
        
        assertTrue(cell1.getCellCondition()==CellCondition.OBSTACLE);
        assertTrue(cell2.getCellCondition()==CellCondition.OPEN);
        assertTrue(cell3.getCellCondition()==CellCondition.STAIRS);
        assertTrue(cell4.getCellCondition()==CellCondition.UNKNOWN);
    }
    
    /**
     * Test of getLocationX method, of class Cell.
     */
    
    public void testGetLocationX() {
        System.out.println("getLocationX");
        Cell cell1 = new Cell(1,0);
        assertTrue(cell1.getLocationX()==1);
    }
    
    /**
     * Test of setLocationX method, of class Cell.
     */
    public void testSetLocationX() {
        System.out.println("setLocationX");
        Cell instance = new Cell();
        instance.setLocationX(1);
        assertTrue(instance.getLocationX()==1);
    }
    
    /**
     * Test of getLocationY method, of class Cell.
     */
    public void testGetLocationY() {
        System.out.println("getLocationY");
        Cell cell1 = new Cell(1,2);
        assertTrue(cell1.getLocationY()==2);
    }

    /**
     * Test of setLocationY method, of class Cell.
     */
    public void testSetLocationY() {
        System.out.println("setLocationY");
        Cell instance = new Cell();
        instance.setLocationY(5);
        assertTrue(instance.getLocationY()==5);
    }
    
    /**
     * Test of getCondition method, of class Cell.
     */
    public void testGetCondition() {
        System.out.println("getCondition");
        
        Cell cell1 = new Cell(1,0, CellCondition.OBSTACLE, "bare", true, false);
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell3 = new Cell(3,0, CellCondition.STAIRS, "HIGH_PILE", true, false);
        Cell cell4 = new Cell(4,0, CellCondition.UNKNOWN, "HIGH_PILE", true, false);
        
        assertTrue(cell1.getCondition()==CellCondition.OBSTACLE);
        assertTrue(cell2.getCondition()==CellCondition.OPEN);
        assertTrue(cell3.getCondition()==CellCondition.STAIRS);
        assertTrue(cell4.getCondition()==CellCondition.UNKNOWN);
    }

    /**
     * Test of setCondition method, of class Cell.
     */
    public void testSetCondition() {
        System.out.println("setCondition");
        Cell cell1 = new Cell(1,0);
       
        cell1.setCondition(CellCondition.OBSTACLE);
        assertTrue(cell1.getCondition()==CellCondition.OBSTACLE);
        
        cell1.setCondition(CellCondition.OPEN);
        assertTrue(cell1.getCondition()==CellCondition.OPEN);
        
        cell1.setCondition(CellCondition.STAIRS);
        assertTrue(cell1.getCondition()==CellCondition.STAIRS);
        
        cell1.setCondition(CellCondition.UNKNOWN);
        assertTrue(cell1.getCondition()==CellCondition.UNKNOWN);
    }
    
    /**
     * Test of getFloorType method, of class Cell.
     */
    public void testGetFloorType() {
        System.out.println("getFloorType");
        Cell cell1 = new Cell(1,0, CellCondition.OPEN, "bare", true, false);
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell3 = new Cell(3,0, CellCondition.OPEN, "HIGH_PILE", true, false);
                
        assertTrue(cell1.getFloorType()==FloorType.bare);
        assertTrue(cell2.getFloorType()==FloorType.LOW_PILE);
        assertTrue(cell3.getFloorType()==FloorType.HIGH_PILE); 
    }
}
