/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package depaul.se459.navigator;

import depaul.se459.controller.VacuumController;
import java.util.ArrayList;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author Sarica
 */
public class LayoutTest extends TestCase {
    VacuumController vc;
    Layout instance; 
        
    public LayoutTest(String name) {
        super(name);
    }
    
    public void setUp() {
        vc=new VacuumController();
        instance = new Layout(vc);                
    }
    
    public void tearDown() {
        instance=null;
        vc=null;
    }   

     /**
     * Test of getLayoutList method, of class Layout.
     */
    
    /*public void testGetLayoutList() {
        System.out.println("getLayoutList");
        Layout instance = null;
        ArrayList<Cell> expResult = null;
        ArrayList<Cell> result = instance.getLayoutList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    
    /**
     * Test of getCurrentCell method, of class Layout.
     */
    public void testGetCurrentCell() {
        System.out.println("getCurrentCell");
        Cell cell1 = new Cell(1,0);
        
        instance.setCurrentCell(cell1);
        assertTrue(instance.getCurrentCell()==cell1);
    }

    /**
     * Test of setCurrentCell method, of class Layout.
     */
    public void testSetCurrentCell() {
        System.out.println("setCurrentCell");
        Cell cell1 = new Cell(1,0);
        
        instance.setCurrentCell(cell1);
        assertTrue(instance.getCurrentCell()==cell1);
    }

    /**
     * Test of getPreviousCell method, of class Layout.
     */
    public void testGetPreviousCell() {
        System.out.println("getPreviousCell");
        Cell cell1 = new Cell(1,0);
        
        instance.setPreviousCell(cell1);
        assertTrue(instance.getPreviousCell()==cell1);
    }

    /**
     * Test of setPreviousCell method, of class Layout.
     */
    public void testSetPreviousCell() {
        System.out.println("setPreviousCell");
        Cell cell1 = new Cell(1,0);
        
        instance.setPreviousCell(cell1);
        assertTrue(instance.getPreviousCell()==cell1);
    }

    /**
     * Test of getPath method, of class Layout.
     */
    /*public void testGetPath() {
        System.out.println("getPath");
        Layout instance = null;
        Graph expResult = null;
        Graph result = instance.getPath();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    
    
    /**
     * Test of getChargingStationsList method, of class Layout.
     */
    public void testGetChargingStationsList() {
        System.out.println("getChargingStationsList");
        Cell cell1 = new Cell(1,0, CellCondition.OPEN, "bare", true, true);
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", true, false);
        instance.getLayoutList().add(cell1);
        instance.getLayoutList().add(cell2);
        
        ArrayList<Cell> result=instance.getChargingStationsList();
        assertTrue(result.size()==1);
    }
    
    /**
     * Test of closestPathChrgnStation method, of class Layout.
     */
    /*@Test
    public void testClosestPathChrgnStation() {
        System.out.println("closestPathChrgnStation");
        Cell cell1 = new Cell(1,0, CellCondition.OPEN, "bare", true, true);         //charging station
        Cell cell2 = new Cell(2,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell3 = new Cell(3,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell4 = new Cell(4,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell5 = new Cell(5,0, CellCondition.OPEN, "LOW_PILE", true, false);
        Cell cell6 = new Cell(6,0, CellCondition.OPEN, "LOW_PILE", true, true);     //charging station
       
        instance.getLayoutList().add(cell1);
        instance.getLayoutList().add(cell2);
        instance.getLayoutList().add(cell3);
        instance.getLayoutList().add(cell4);
        instance.getLayoutList().add(cell5);
        instance.getLayoutList().add(cell6);
        
        instance.setCurrentCell(cell3);
        ArrayList<Cell> result = instance.closestPathChrgnStation(cell3);
        System.out.println(result.size());
        assertTrue(result.size()==3);
    }
    */
    /**
     * Test of getCurrentLayout method, of class Layout.
     */
    //Sarica: This method's implementation is retuning null. Hence no test is needed.
    
    /*public void testGetCurrentLayout() {
        System.out.println("getCurrentLayout");
        Layout instance = null;
        ArrayList<Cell> expResult = null;
        ArrayList<Cell> result = instance.getCurrentLayout();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
}
