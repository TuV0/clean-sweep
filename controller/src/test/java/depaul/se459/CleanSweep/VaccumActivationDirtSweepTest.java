package depaul.se459.CleanSweep;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.clean_dirt.VacuumEngine;
import depaul.se459.sensor.Sensor;
import depaul.se459.sensor.SensorCell;

/**
 * A test class associated with user story 4.1:
 * 
 * I, as a user, want the control system to engage the vacuum and sweep up dirt. 
 * @author joerichard
 *
 */
public class VaccumActivationDirtSweepTest extends TestCase {
		public VaccumActivationDirtSweepTest(String name){
			super(name);
		}
		
		
		/**
		 * The goal of this test is to make sure that the vacuum activates when it detects dirt and cleans that cell until it is clean. 
		 * What is tested here:
		 * - The vacuum engine can detect dirt.
		 * - The vacuum engine can remove dirt from a cell. 
		 * - The vacuum engine can detect the absence of dirt. 
		 */
		public void testVaccumActivationAndSweeping(){
			
			/*Sensor sense  = Sensor.getInstance();
			sense.loadDataToSensor();
			List<SensorCell> c = sense.getCellList();
			VacuumEngine engine = new VacuumEngine(sense);
			for(SensorCell sc: c){
				boolean isDirty = engine.isDirty(sc.getCordX(), sc.getCordY());
				while(isDirty){
					engine.extractDirt(sc.getCordX(), sc.getCordY());
					isDirty = engine.isDirty(sc.getCordX(), sc.getCordY());
				}
				
			}
			for(SensorCell sc: c){
				Assert.assertTrue(sc.getUnitOfDirt()==0);
			}*/
			
		}

}
