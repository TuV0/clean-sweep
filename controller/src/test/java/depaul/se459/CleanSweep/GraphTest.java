package depaul.se459.CleanSweep;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.navigator.*;
import depaul.se459.util.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
public class GraphTest extends TestCase {
	public GraphTest(String name){
		super(name);
	}
	public void testAddEdge(){
		Graph graph = new Graph();
		Cell c1 = new Cell(0,0);
		Cell c2 = new Cell(0,1);
		graph.addEdge(c1,c2);
		
		assertTrue(graph.memberOf(c1));
		assertTrue(graph.memberOf(c2));
	}
	public void testGetNeighbors(){
		Graph graph = new Graph();
		Cell c1 = new Cell(0,0);
		Cell c2 = new Cell(0,1);
		Cell c3 = new Cell(1,1);
		Cell c4 = new Cell(1,2);
		Cell c5 = new Cell(2,2);
		graph.addEdge(c1,c2);
		graph.addEdge(c2,c3);
		graph.addEdge(c3,c4);
		graph.addEdge(c4,c5);
		graph.addEdge(c2,c4);
		
		ArrayList<Cell> cellList = graph.getNeighbours(c2);
		
		for(int i=0; i<cellList.size();i++){
			if (cellList.get(i) != null){
				if(cellList.get(i).getLocationX() == c1.getLocationX() && cellList.get(i).getLocationY() == c1.getLocationY()){
					assertTrue(cellList.get(i).getLocationX() == c1.getLocationX() && cellList.get(i).getLocationY() == c1.getLocationY());
				}
				else if(cellList.get(i).getLocationX() == c4.getLocationX() && cellList.get(i).getLocationY() == c4.getLocationY()){
					assertTrue(cellList.get(i).getLocationX() == c4.getLocationX() && cellList.get(i).getLocationY() == c4.getLocationY());
				}
				else if(cellList.get(i).getLocationX() == c3.getLocationX() && cellList.get(i).getLocationY() == c3.getLocationY()){
					assertTrue(cellList.get(i).getLocationX() == c3.getLocationX() && cellList.get(i).getLocationY() == c3.getLocationY());
				}
				else{
					assertTrue(1 == 0);
				}
			}
		}
		
	}
}
