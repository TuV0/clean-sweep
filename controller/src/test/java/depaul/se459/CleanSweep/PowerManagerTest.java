package depaul.se459.CleanSweep;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.navigator.*;
import depaul.se459.util.*;
import depaul.se459.state.*;
import depaul.se459.controller.*;
import depaul.se459.powermanagement.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PowerManagerTest extends TestCase {
	public PowerManagerTest(String name){
		super(name);
	}
	public void testCharge(){
		VacuumController vc = new VacuumController();
		PowerManager pm = new PowerManager(vc);
		VacuumState vs = VacuumState.getInstance();
		
		vs.setBatteryLife(5.0);
		assertTrue(vs.getBatteryLife() == 5.0);
		
		pm.charge();
		
		assertTrue(vs.getBatteryLife() == 100);
	}
	
	public void testSubtractBattery(){
		VacuumController vc = new VacuumController();
		PowerManager pm = new PowerManager(vc);
		VacuumState vs = VacuumState.getInstance();
		
		vs.setBatteryLife(50.0);
		assertTrue(vs.getBatteryLife() == 50.0);
		
		pm.substractBattery(10.0);
		assertTrue(vs.getBatteryLife() == 40.0);
		pm.substractBattery(20.0);
		assertTrue(vs.getBatteryLife() == 20.0);
	}
	
}
