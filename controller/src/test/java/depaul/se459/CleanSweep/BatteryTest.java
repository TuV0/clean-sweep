package depaul.se459.CleanSweep;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.navigator.*;
import depaul.se459.util.*;
import depaul.se459.state.*;
import depaul.se459.controller.*;
import depaul.se459.powermanagement.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
public class BatteryTest extends TestCase {
	public BatteryTest(String name){
		super(name);
	}
	public void testSubtractBattery(){
		VacuumController controller = new VacuumController();
		Battery battery = new Battery(controller);
		battery.setRemainingBattery(100.0);
		battery.substractBattery(10.0);
		assertTrue(battery.getRemainingBattery() == 90.0);
		
	}
	public void testGetSetRemainingBattery(){
		VacuumController controller = new VacuumController();
		Battery battery = new Battery(controller);
		battery.setRemainingBattery(100.0);
		assertTrue(battery.getRemainingBattery() == 100.0);
	}
	public void testGetSetBatteryLife(){
		VacuumController controller = new VacuumController();
		VacuumState vacuumState = VacuumState.getInstance();
		Battery battery = new Battery(controller);
		vacuumState.setBatteryLife(4.0);
		assertTrue(battery.getBatteryLife() == 4.0);
	}
}
