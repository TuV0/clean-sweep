package depaul.se459.CleanSweep;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.Assert;

import java.util.List;

import depaul.se459.navigator.*;
import depaul.se459.util.*;
import depaul.se459.state.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class VacuumStateTest extends TestCase {
	public VacuumStateTest(String name){
		super(name);
	}
	public void testGetSetBatteryLife(){
		VacuumState vState = VacuumState.getInstance();
		
		vState.setBatteryLife(2);
		
		assertTrue(vState.getBatteryLife() == 2);
	}
	public void testGetSetDirtCapacity(){
		VacuumState vState = VacuumState.getInstance();
		
		//assertTrue(vState.getDirtCapacityState() == 0);
		
		vState.resetDirtCapacityState(10);
		assertTrue(vState.getDirtCapacityState() == 10);
	}
	public void testGetSetEmptyIndicatorState(){
		VacuumState vState = VacuumState.getInstance();
		
		assertTrue(vState.getEmptyIndicatorState() == false);
		
		vState.setEmptyIndicatorState(true);
		assertTrue(vState.getEmptyIndicatorState() == true);
	}
	
	public void testGetSetCurrentCell(){
		VacuumState vState = VacuumState.getInstance();
		Cell c1 = new Cell(0,0);
		Cell c2 = new Cell(0,2);
		//assertTrue(vState.getCurrentCell() == null);
		vState.setCurrentCell(c1);
		
		assertTrue(vState.getCurrentCell() == c1);
		vState.setCurrentCell(c2);
		assertTrue(vState.getCurrentCell() == c2);
	}
	public void testGetSetPreviousCell(){
		VacuumState vState = VacuumState.getInstance();
		Cell c1 = new Cell(0,0);
		Cell c2 = new Cell(0,2);
		assertTrue(vState.getPreviousCell() == null);
		vState.setPreviousCell(c1);
		
		assertTrue(vState.getPreviousCell() == c1);
		vState.setPreviousCell(c2);
		assertTrue(vState.getPreviousCell() == c2);
	}
}
