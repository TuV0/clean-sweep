package depaul.se459.util;

import java.util.ArrayList;
import java.util.Iterator;

import depaul.se459.navigator.Cell;
import depaul.se459.util.BreadthFirstSearch;
import depaul.se459.util.Graph;
import junit.framework.TestCase;

/**
 * 
 * @author tuvo
 *
 */

public class ShortestPathTest extends TestCase {
	
	
	
	public static void testShortesPath(){
		ArrayList<Cell> path = new ArrayList<Cell>();
		
		Cell cell1 = new Cell(0,0);
		Cell cell2 = new Cell(1,0);
		Cell cell3 = new Cell(2,0);
		Cell cell4 = new Cell(3,0);
		
		Cell cell5 = new Cell(0,1);
		Cell cell6 = new Cell(1,1);
		Cell cell7 = new Cell(2,1);
		Cell cell8 = new Cell(3,1);
		
		Cell cell9 = new Cell(3,2);
		Cell cell10 = new Cell(2,2);

		
		Graph graph = new Graph();
	    
		graph.addEdge(cell1, cell2);
		graph.addEdge(cell2, cell1);

		graph.addEdge(cell2, cell3);
		graph.addEdge(cell3, cell2);

		graph.addEdge(cell3, cell4);
		graph.addEdge(cell4, cell3);		

		graph.addEdge(cell4, cell8);
		graph.addEdge(cell8, cell4);

		graph.addEdge(cell8, cell9);
		graph.addEdge(cell9, cell8);

		graph.addEdge(cell9, cell10);
		graph.addEdge(cell10, cell9);


/*
		graph.addEdge(cell1, cell5);
		graph.addEdge(cell5, cell6);
		graph.addEdge(cell6, cell7);
		graph.addEdge(cell7, cell10);
		
		graph.addEdge(cell10, cell7);

		graph.addEdge(cell7, cell6);
		graph.addEdge(cell6, cell5);*/

		
		

		//path = graph.getNeighbours(cell1);
		path = BreadthFirstSearch.breadthFirstSearch(graph, cell10, cell3);
		
		Iterator<Cell> iterator = path.iterator();
		while (iterator.hasNext()) {
			Cell cell = iterator.next();
			System.out.println("x: " + cell.getLocationX() + "y: " + cell.getLocationY());
		}

		
		
	}
	
	
}
