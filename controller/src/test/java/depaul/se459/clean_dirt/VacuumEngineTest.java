/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package depaul.se459.clean_dirt;

import depaul.se459.controller.VacuumController;
import depaul.se459.navigator.Cell;
import depaul.se459.navigator.CellCondition;
import depaul.se459.sensor.FloorType;
import depaul.se459.sensor.Sensor;
import depaul.se459.sensor.SensorCell;
import static junit.framework.Assert.assertTrue;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 *
 * @author Sarica
 */
public class VacuumEngineTest  extends TestCase {
     VacuumController vc ;
     VacuumEngine instance;
   
    public VacuumEngineTest(String name) {
        super(name);
    }
    
    @Override
    public void setUp() {
        vc=new VacuumController();
        instance = vc.getVacuumEngine();
    }
    
    @Override
    public void tearDown() {
        vc=null;
        instance=null;
    }
    
    public static junit.framework.Test suite(){
        return new TestSuite(VacuumEngineTest.class);
    }
    
    /**
     * Test of cleanIfDirty method, of class VacuumEngine.
     */
    public void testCleanIfDirty() {
       
        System.out.println("cleanIfDirty");
        Cell cell = new Cell(1,0, CellCondition.OPEN, "bare", true, false);
        
        double initialBattery = instance.getPowerManager().getVacuumState().getBatteryLife();
        
        SensorCell sc = instance.getSensor().getCellFromSensor(1,0);
	double amount =  sc.getUnitOfDirt();
        
        boolean cellInitialStatus=instance.isDirty(cell);
        instance.cleanIfDirty(cell);
        
        double batteryAfterCall = instance.getPowerManager().getVacuumState().getBatteryLife();
//        assertTrue(cell.getCellWeight()+"Before call=> Cell(1,0):"+cellInitialStatus+", battery:"+initialBattery +".   After call=> Cell(1,0):"+instance.isDirty(cell)+",  battery:"+batteryAfterCall, (instance.isDirty(cell)==false) && (batteryAfterCall==initialBattery-amount)); 
    }

    /**
     * Test of isDirty method, of class VacuumEngine.
     */
    public void testIsDirty() {
        
        System.out.println("isDirty");
        Cell cell = new Cell(1,0);
        
        boolean expResult;
        
        SensorCell sc = instance.getSensor().getCellFromSensor(1,0);
        if(sc.getUnitOfDirt() > 0)
               expResult=true;
        else
                expResult=false;
        
        boolean result = instance.isDirty(cell);
        assertTrue("expResult:"+expResult+" result:"+result, expResult==result);       
    }


    
 /**
     * Test of extractDirt method, of class VacuumEngine.
     */
    
//    public void testExtractDirt() {
//        
//        System.out.println("extractDirt");
//        Cell cell = new Cell(1,0, CellCondition.OPEN, "bare", true, false);
//        
//        double initialBattery = instance.getPowerManager().getVacuumState().getBatteryLife();
//        
//        SensorCell sc = instance.getSensor().getCellFromSensor(1,0);
//	double initialDirtAmount =  sc.getUnitOfDirt();
//        System.out.println("initialDirtAmount:"+initialDirtAmount);
//        
//        boolean cellInitialStatus=instance.isDirty(cell);
//        instance.extractDirt(cell);
//        
//        sc = instance.getSensor().getCellFromSensor(1,0);
//	double finalDirtAmountActual =  sc.getUnitOfDirt();
//        
//        double batteryAfterCallActual = instance.getPowerManager().getVacuumState().getBatteryLife();
//        
//        double finalDirtAmountExpected=initialDirtAmount-1;
//        double batteryAfterCallExpected=initialBattery-1;
//        
//        if(initialDirtAmount==0){
//            finalDirtAmountExpected=0;
//            batteryAfterCallExpected=initialBattery;
//        }
//        
//        assertTrue("cellInitialStatus:"+cellInitialStatus+" finalDirtAmtActual:"+finalDirtAmountActual+", batteryAfrCallActual:"+batteryAfterCallActual +".finalDirtAmtExp:"+finalDirtAmountExpected+",  batteryAfterCallExpected:"+batteryAfterCallExpected, (finalDirtAmountActual==finalDirtAmountExpected) && (batteryAfterCallActual==batteryAfterCallExpected));               
//    }
   
    /**
     * Test of getCurrentDirtAmount method, of class VacuumEngine.
     */
    public void testGetCurrentDirtAmount() {
        VacuumController vc ;
        vc=new VacuumController();
        System.out.println("getCurrentDirtAmount");
        VacuumEngine instance = vc.getVacuumEngine();
        int expResult = 1;
        int dirtUnit = 1;
        instance.setCurrentDirtAmount(dirtUnit);
        int result = instance.getCurrentDirtAmount();
        instance=null;
        vc=null;
        assertTrue("ExpResult: "+expResult+" result:"+result,expResult==result);                       
    }
    
    /**
     * Test of setCurrentDirtAmount method, of class VacuumEngine.
     */
    public void testSetCurrentDirtAmount() {
        System.out.println("setCurrentDirtAmount");
        VacuumController vc;
        vc=new VacuumController();
        VacuumEngine instance = vc.getVacuumEngine();
        int dirtUnit = 1;
        instance.setCurrentDirtAmount(dirtUnit);
        int currentDirtAmountActual=instance.getCurrentDirtAmount();
        instance=null;
        vc=null;
        assertTrue("instance.getCurrentDirtAmount(): "+currentDirtAmountActual,currentDirtAmountActual==1);
    }
    
    /**
     * Test of getEmptyIndicator method, of class VacuumEngine.
     */
    public void testGetEmptyIndicator() {
        System.out.println("getEmptyIndicator");
        VacuumController vc ;
        vc=new VacuumController();
        VacuumEngine instance = vc.getVacuumEngine();
        boolean expResult = false;
        boolean result = instance.getEmptyIndicator();
        instance=null;
        vc=null;
        assertTrue("result: "+result,expResult==result); 
    }
    
    /**
     * Test of setEmptyIndicatorS method, of class VacuumEngine.
     */
    public void testSetEmptyIndicatorS() {
        System.out.println("setEmptyIndicatorS");
        VacuumController vc ;
        vc=new VacuumController();
        VacuumEngine instance = vc.getVacuumEngine();
        boolean emptyState = false;
        instance.setEmptyIndicatorS(emptyState);
        boolean result = instance.getEmptyIndicator();
        instance=null;
        vc=null;
        assertTrue("result: "+result,emptyState==result); 
    }
}
